package leopard.repository;

import leopard.domain.LikeObj;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LikeRepository extends CrudRepository<LikeObj, Long> {
}
